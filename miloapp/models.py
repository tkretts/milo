from random import randint

from django.db import models
from django.contrib.auth.models import User


def rnd():
    """ Generate random default value """
    return randint(1, 100)


class MiloUser(User):
    """ User model class """

    birthday = models.DateField(verbose_name='Birthday')
    rnd = models.IntegerField(verbose_name='Random', default=rnd)

    def __str__(self):
        """ Displayable field """
        return self.username
