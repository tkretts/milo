""" Miloapp template tags

    @author: as.bogunkov
"""
from datetime import date

from django import template

register = template.Library()


@register.simple_tag
def eligible(user):
    """ Is user allowed or blocked
    """
    return ['blocked', 'allowed'][(date.today() - user.birthday).days / 365.25 > 13]


@register.simple_tag
def bizzfuzz(user):
    """ BizzFuzz specification
    """
    return ''.join([
        not user.rnd % 3 and 'Bizz' or '',
        not user.rnd % 5 and 'Fuzz' or '',
    ])
