from django.apps import AppConfig


class MiloappConfig(AppConfig):
    name = 'miloapp'
