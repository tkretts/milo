from datetime import date, timedelta

from django.test import TestCase

from miloapp.models import MiloUser
from miloapp.tags import eligible, bizzfuzz


class MiloappTestCase(TestCase):
    """ Test miloapp helpers """

    def setUp(self):
        MiloUser.objects.create(username='Test1', birthday=date.today() - timedelta(days=365 * 15), rnd=14)
        MiloUser.objects.create(username='Test2', birthday=date.today() - timedelta(days=365 * 10), rnd=6)
        MiloUser.objects.create(username='Test3', birthday=date(1985, 5, 13), rnd=10)
        MiloUser.objects.create(username='Test4', birthday=date(1985, 5, 13), rnd=15)

    def test_eligible(self):
        """ Test eligible template tag """
        old = MiloUser.objects.get(username='Test1')
        young = MiloUser.objects.get(username='Test2')
        self.assertEqual(eligible(old), 'allowed', msg='User must be allowed.')
        self.assertEqual(eligible(young), 'blocked', msg='User must not be allowed.')

    def test_bizzfuzz(self):
        """ Test bizzfuzz template tag """
        no_bizz = MiloUser.objects.get(username='Test1')
        has_bizz = MiloUser.objects.get(username='Test2')
        has_fuzz = MiloUser.objects.get(username='Test3')
        has_both = MiloUser.objects.get(username='Test4')
        self.assertEqual(bizzfuzz(no_bizz), '', msg='User must have no BizzFuzz.')
        self.assertEqual(bizzfuzz(has_bizz), 'Bizz', msg='User must have Bizz.')
        self.assertEqual(bizzfuzz(has_fuzz), 'Fuzz', msg='User must have Fuzz.')
        self.assertEqual(bizzfuzz(has_both), 'BizzFuzz', msg='User must have BizzFuzz.')
