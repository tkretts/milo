import csv
from datetime import date

from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from django.forms.widgets import SelectDateWidget

from miloapp.models import MiloUser
from miloapp.tags import eligible, bizzfuzz


def download(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'

    writer = csv.writer(response)
    writer.writerow(['Username', 'Birthday', 'Eligible', 'Random', 'BizzFuzz'])
    for row in MiloUser.objects.all():
        writer.writerow([row.username, row.birthday, eligible(row), row.rnd, bizzfuzz(row)])

    return response


class UsersListView(ListView):
    """ Index page view """
    model = MiloUser
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CreateUserView(CreateView):
    """ Add user view """
    model = MiloUser
    fields = ['username', 'birthday']
    template_name = 'create.html'
    success_url = reverse_lazy('home')

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['birthday'].widget = SelectDateWidget(
            years=reversed(range(date.today().year - 150, date.today().year)))
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action_type'] = 'Create'
        return context


class ShowUserView(DetailView):
    """ Show user data view """
    model = MiloUser
    template_name = 'view.html'


class UpdateUserView(UpdateView):
    """ Edit user view """
    model = MiloUser
    fields = ['username', 'birthday']
    template_name = 'create.html'
    success_url = reverse_lazy('home')

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['birthday'].widget = SelectDateWidget(
            years=reversed(range(date.today().year - 150, date.today().year)))
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action_type'] = 'Update'
        return context


class DeleteUserView(DeleteView):
    """ Delete user """
    model = MiloUser
    template_name = 'delete.html'
    success_url = reverse_lazy('home')
