""" Miloapp urls

    @author: as.bogunkov
"""
from django.urls import path

from miloapp.views import UsersListView, CreateUserView, UpdateUserView, ShowUserView, DeleteUserView, download

urlpatterns = [
    path('', UsersListView.as_view(), name='home'),
    path('create/', CreateUserView.as_view(), name='create-user'),
    path('edit/<int:pk>/', UpdateUserView.as_view(), name='edit-user'),
    path('view/<int:pk>/', ShowUserView.as_view(), name='view-user'),
    path('delete/<int:pk>/', DeleteUserView.as_view(), name='delete-user'),
    path('download/', download, name='download'),
]
